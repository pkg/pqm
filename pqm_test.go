package main

import "testing"
import (
	"database/sql"
	"encoding/json"

	_ "github.com/lib/pq"
)
import "log"

var dbx *sql.DB

// some sanity checks
var _ json.Unmarshaler = &NullString{}
var _ json.Marshaler = &NullString{}

func db() *sql.DB {
	var err error
	if dbx == nil {
		dbx, err = sql.Open("postgres", "dbname=pqm_test sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
	}
	return dbx
}

func TestPersonHasNameField(t *testing.T) {
	return
}

func TestScanBasics(t *testing.T) {
	rows, err := db().Query(`SELECT * FROM person WHERE name = 'Jeff'`)
	if err != nil {
		t.Fatal(err)
	}
	count := 0
	for rows.Next() {
		var p Person
		err = p.Scan(rows)
		if err != nil {
			t.Fatal(err)
		}
		if p.Name.Val != "Jeff" {
			t.Error("expected p.Name to be Jeff")
		}
		if p.SecondName.Val != "Jefferson" {
			t.Error("expected p.SecondName to be Jefferson")
		}
		if p.Age.Val != 10 {
			t.Error("expected p.Age to be 10")
		}
		if p.ExactAge.Val != 9.75 {
			t.Error("expected p.ExactAge to be 9.75")
		}
		count++
	}
	err = rows.Err()
	if err != nil {
		t.Fatal(err)
	}
	if count == 0 {
		t.Fatal("expected there to be some Person rows named Jeff to test against")
	}
}

func getPerson(name string, t *testing.T) *Person {
	res, err := GetPeople(db(), "WHERE name = $1 LIMIT 1", name)
	if err != nil {
		t.Fatal(err)
	}
	if len(res) == 0 {
		t.Fatalf("expected there to be some Person rows named %s to test against", name)
	}
	return res[0]
}

func TestUpdateBasics(t *testing.T) {
	// setup
	p := getPerson("Jeff", t)
	if !p.UpdateMe.Null {
		t.Fatal("expected UpdateMe field to be Null")
	}
	// update
	p.UpdateMe.Scan("updated")
	p.db = db()
	err := p.Update()
	if err != nil {
		t.Fatal(err)
	}
	// reload
	p = getPerson("Jeff", t)
	if p.UpdateMe.String() != "updated" {
		t.Fatal("expected UpdateMe field to be updated got:" + p.UpdateMe.String())
	}
}

func TestInsertBasics(t *testing.T) {
	// setup
	p := new(Person)
	p.Id.Null = true
	p.Name.Val = "Bob"
	p.ClubId.Null = true
	p.db = db()
	err := p.Insert()
	if err != nil {
		t.Fatal(err)
	}
	p = getPerson("Bob", t)
	if p.Name.Val != "Bob" {
		t.Fatalf("expected new person's Name field to be Bob got: %s", p.Name)
	}
}

func TestDeleteBasics(t *testing.T) {
	// setup
	p := new(Person)
	p.Id.Null = true
	p.Name.Val = "Doomed"
	p.Name.Null = false
	p.ClubId.Null = true
	p.db = db()
	err := p.Insert()
	if err != nil {
		t.Fatal(err)
	}
	p = getPerson(p.Name.Val, t)
	if p.Name.Val != "Doomed" {
		t.Fatalf("expected new person's Name field to be Doomed got: %s", p.Name)
	}
	// kill em
	p.db = db()
	err = p.Delete()
	if err != nil {
		t.Fatal(err)
	}
	row := db().QueryRow("select count(*) from person where name = $1", "Doomed")
	var cnt int
	err = row.Scan(&cnt)
	if err != nil {
		t.Fatal(err)
	}
	if cnt > 0 {
		t.Error("Expected to delete the record named 'Doomed'")
	}

}

func TestNullString(t *testing.T) {
	rows, err := db().Query(`SELECT * FROM person WHERE nick_name IS NULL`)
	if err != nil {
		t.Fatal(err)
	}
	count := 0
	for rows.Next() {
		var p Person
		err = p.Scan(rows)
		if err != nil {
			t.Fatal(err)
		}
		if !p.NickName.Null {
			t.Error("expected p.NickName to be null")
		}
		count++
	}
	err = rows.Err()
	if err != nil {
		t.Fatal(err)
	}
	if count == 0 {
		t.Fatal("expected there to be some null Person rows to test against")
	}
	return
}

// ensure UnmarshalJSON works
func TestNullStringJSON(t *testing.T) {
	p := new(Person)
	err := p.NickName.UnmarshalJSON([]byte(`"Jeffers"`))
	if err != nil {
		t.Fatal(err)
	}
	if p.NickName.Null {
		t.Error("expected NullString to have a value")
	}
	if p.NickName.String() != "Jeffers" {
		t.Errorf("expected NullString field to get Unmashaled from JSON got:" + p.NickName.String())
	}
}

func TestHasOneBasics(t *testing.T) {
	// setup
	p := getPerson("Grandmaster", t)
	c, err := p.GetClub()
	if err != nil {
		t.Fatal(err)
	}
	if c.Name.Val != "Chess" {
		t.Fatalf("expected Grandmaster's related Club to be 'Chess' but got: %s", c.Name)
	}
	// test inverse (has_many) relationship
	people, err := c.GetPeople()
	if err != nil {
		t.Fatal(err)
	}
	if len(people) != 1 {
		t.Fatal("expected Chess Club to have exactly 1 related person got: %d", len(people))
	}
}

func TestSchemaCheck(t *testing.T) {
	err := checkSchema(db())
	if err != nil {
		t.Error("expected checkSchema to pass")
	}
	_, err = db().Exec(`ALTER TABLE person ADD COLUMN height integer`)
	if err != nil {
		t.Fatal(err)
	}
	err = checkSchema(db())
	if err == nil {
		t.Error("expected checkSchema to fail after changing postgres schema")
	}
	_, err = db().Exec(`ALTER TABLE person DROP COLUMN height`)
	if err != nil {
		t.Fatal(err)
	}
	err = checkSchema(db())
	if err != nil {
		t.Error("expected checkSchema to pass after changing postgres schema back again")
	}
	return
}
