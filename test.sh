#!/bin/bash -e
#
# Description:
#   Tests output by creating a database/schema running pqm on it
#   then checking the generated models.
#
# Usage:
#   PGHOST=localhost PGUSER=postgres ./test.sh
#

DBARGS="pqm_test"
TMP="pqm_model_test.go"

dropdb $DBARGS || echo "ignored"
createdb $DBARGS
psql $DBARGS < pqm_test_schema.psql

rm -f pqm_model_test.go
go build && ./pqm -U $USER -h $PGHOST -d pqm_test > $TMP

go test $TMP pqm_test.go
