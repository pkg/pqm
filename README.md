pqm
===

Go tool to generate go boilerplate code for working with a postgres database.

The idea is to remove the hidden "magic" used by ORM-type libraries, by pre-compiling types for each of your database tables rather than relying on reflection techniques.

The tool will generate some helper code that you will then include in your application and use alongside the standard `database/sql` package.


Example
-------

So say you have a PostgreSQL schema like:

```

CREATE TABLE person (
	id SERIAL PRIMARY KEY,
	first_name text NOT NULL DEFAULT '',
	second_name text NOT NULL DEFAULT ''
);

```

You would then use the `pqm` tool to generate a `.go` file to include in your project which would look something like:

```Go

// ------8<------- snip some support funcs

type Person struct {
	Id int64
	Name string
	SecondName string
}

func (m *Person) Scan(rows *sql.Rows) error {
	return scanModel(rows, m)
}


```

You and would then be able to conviniently `Scan` into the generated struct files using the `database/sql` package and the generated helper fucntions like:

```Go

	rows,_ := db().Query("SELECT * FROM person")
	for rows.Next() {
		var p Person
		p.Scan(rows)
		// use p.FirstName
		// use p.SecondName
		// etc
	}
```


Installation
------------

Quick installation with `go get`

`go get github.com/chrisfarms/pqm`


Status
------

Only a few hours work so far... just an idea, but soon to add:

* helpers for INSERTing the types
* helpers for UPDATEing the types
* larger support for column type mapping
* a way of checking if the generated file being used is out of sync with db-schema
* less picky about col names
